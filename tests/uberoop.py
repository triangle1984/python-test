class Private:
    def _test():
        print("нет уж")
    def __test():
        print("гыы")
class A:
    xc = 3333
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def method(self):
        print(self.x + self.y)
class B(A):
    def method(self):
        print("ы")
class A2:
    def test(self):
        print("ыпыпып")
        self.x = 4
class A3(A2):
    def test(self):
        A2.test()
        print("ок")
class Statistest:
    @classmethod
    def test(cls, func):
        cls.func = func
def test():
    print(".")
Statistest.test(test)
Statistest.func()
