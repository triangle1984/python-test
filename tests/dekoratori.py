#!/usr/bin/env python
def dekorator(func):
    def accept():
        print("запускаем функцию ", func)
        func()
        print("останавливаем")
    def test():
        print("ноибал, не останавливааем")

    return accept
@dekorator
def test():
    print("ку я функция")
@dekorator
def test2():
    print("ку я функция2")
test2()

