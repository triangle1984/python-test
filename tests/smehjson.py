import json, base64
test = {'x':3,
        'y':4}
params = json.dumps(test)
result = base64.b64encode(bytes(params, 'utf-8')).decode('utf-8')
print(result)
result = base64.b64decode(result).decode('utf-8')
result = json.loads(result)
