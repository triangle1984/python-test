#!/usr/bin/env python 
from socket import *

#данные сервера
host = 'localhost'
port = 1604
addr = (host,port)

tcp_socket = socket(AF_INET, SOCK_STREAM)
tcp_socket.bind(addr)
tcp_socket.listen(1)

while True:
    print('wait connection...')
    conn, addr = tcp_socket.accept()
    print('client addr: ', addr)
    data = conn.recv(1024)
    if not data:
        conn.close()
        break
    else:
        print(data)
        conn.send(b'Hello from server!')
        continue

tcp_socket.close()

