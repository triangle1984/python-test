#!/usr/bin/env python 
import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(("127.0.0.1", 1604))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print("законектился челик", addr)
        while True:
            data = conn.recv(1024)
            if not data:
                break
            conn.sendall("тест")
