#!/usr/bin/env python
# -*- coding: utf-8 -*-
try:
    import click, random, pyperclip
except ModuleNotFoundError:
    print("Установите модуль pyperclip или click")
    exit(0)
from click import echo
@click.command()
@click.option('--copy', default=False, is_flag=True,
              help="Скопироввать смех, по дефолту false")
@click.option("-c", "--count", default=None,
              type=int,help="Длина смеха, по дефолту рандом 5-35")
@click.option("-s", "--smex", required=False,
              help="символы смеха, необязательный аргумент")
@click.option("-sc", "--smexcount", default=1,
              help="количество смеха")
@click.option("-s2", "--smexslova",
              help="целые слова для смеха")
def main(count, smex, copy, smexcount, smexslova):
    test = 0
    main = ["Х", "Ы", "Ъ"]
    if smex != None:
        main = list(smex)
    if smexslova != None:
        main = smexslova.split()
    mainsmex = []
    for _ in range(smexcount):
        if count == None:
            count = random.randint(5, 35)
        for _ in range(count):
            mainsmex.append(random.choice(main))
            count = None
        mainsmex = "".join(mainsmex)
        if copy:
            pyperclip.copy(mainsmex)
        echo(mainsmex)
        mainsmex = []
main()
