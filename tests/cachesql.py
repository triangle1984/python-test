import pymysql, vk_api, pylibmc
from token2 import ip, tablechat
from vk_api.utils import get_random_id
from pymysql.cursors import DictCursor
from contextlib import closing
from token2 import *
mc = pylibmc.Client(["127.0.0.1"])
def auth():
    conn = pymysql.connect(host=ip,
                             user="root",
                             password="123",
                             db="mydb",
                             cursorclass=DictCursor)
    return conn
def checktable(table, value, should, andd=False):
    conn = auth()
    with conn.cursor() as cursor:
        query = f"SELECT * FROM {table} WHERE {value} = '{should}'"
        if andd:
            query = f"SELECT * FROM {table} WHERE {value} = '{should}' and {andd}"
        cursor.execute(query)
    return cursor.fetchone()
def tableadd(table, value, add, one=False):
    conn = auth()
    try:
        with conn.cursor() as cursor:
            query = f"INSERT INTO {table} ({value}) VALUES ({add})"
            cursor.execute(query)
            conn.commit()
    except pymysql.err.InternalError:
        return
def tablerm(table, value, rm, andd=False):
    conn = auth()
    with conn.cursor() as cursor:
        query = f"DELETE FROM {table} WHERE {value} = '{rm}'"
        if andd:
            query = f"DELETE FROM {table} WHERE {value} = '{rm}' and {andd}"
        cursor.execute(query)
        conn.commit()
if "367919273" in mc:
    print(mc.get("367919273"))
else:
    prefix = checktable("prefix", "id", 367919273)["name"]
    vips = bool(checktable("vips", "id", 367919273))
    admins = bool(checktable("admins", "id", 367919273))
    mc.set("367919273", {"vips":vips, "prefix": prefix, "admins":admins})
