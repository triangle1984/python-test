#!/usr/bin/env python 
class AttDisplay:
    def gatherAttrs(self):
        attrs = []
        for key in sorted(self.__dict__):
            attrs.append("%s=%s" % (key, getattr(self, key)))
        return " , ".join(attrs)

    def __str__(self):
        return "[%s:%s]" % (self.__class__.__name__, self.gatherAttrs())


if __name__ == "__main__":
    class TopTest(AttDisplay):
        count = 1

        def __init__(self):
            self.attr1 = TopTest.count
            self.attr2 = TopTest.count + 1
            TopTest.count += 2


    class Subtest(TopTest):
        pass


    X, Y = TopTest(), Subtest()
    print(X)
    print(Y)
