class Test(object):

    def __init__(self, method=None):
        self._method = method

    def __getattr__(self, method):
        return Test(
            (self._method + '.' if self._method else '') + method
        )
    def __call__(self, **kwargs):
        print(self._method)
        print(kwargs)
test = Test()
test.vk.message.send(uid=4, message="топ")
