#!/usr/bin/env python
import requests, sys
def translate(text):
    apikey = "trnsl.1.1.20190508T201810Z.385ebfa1e596baa0.90672cf8655555b1b51ced31b03c2e8bb9bde46c"
    url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    params = {"key": apikey,
            "text":text,
            "lang":sys.argv[1]}
    r = requests.get(url, params=params)
    encode = r.json()
    encode = encode["text"][0]
    print(encode)
try:
    while 1:
        english = input("Текст: ")
        translate(english)
except (KeyboardInterrupt, EOFError):
    print()
    sys.exit()
