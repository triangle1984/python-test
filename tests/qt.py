#!/usr/bin/env python 
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
class GUI(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
    chislo = ""
    def initUI(self):
        self.setToolTip("падсказка")
        btn = QPushButton("1", self)
        btn1 = QPushButton("2", self)
        btn.setToolTip("чота")
        btn.clicked.connect(lambda:self.chislo +"1")
        btn1.clicked.connect(lambda:self.chislo +"2")
        btn3 = QPushButton("подсчитать", self)
        btn3.clicked.connect(self.calc())
        btn.resize(btn.sizeHint())
        btn.move(50, 50)

        self.setGeometry(300, 300, 200, 200)
        self.setWindowTitle("тест")
        self.show()

    def calc(self):
        self.chislo = int(self.chislo)
        print("Вы набрали" + self.chislo)

app = QApplication(sys.argv)
ex = GUI()
sys.exit(app.exec_())
