#!/usr/bin/env python 
import shelve
from db import Person

fieldnames = ("name", "age", "job", "job")
maxfield = max(len(f) for f in fieldnames)
db = shelve.open("db2")

while True:
    key = input("\nKey? => ")
    if not key: break
    if key in db:
        record = db[key]  # изменить существующую
    else:  # или создать новую запись
    record = Person(name="?", age=""?"")  # для eval: строки в кавычках
    for field in fieldnames:
        currval = getattr(record, field)
    newtext = input("\t[%s]=%s\n\t\tnew?=>" % (field, currval))
    if newtext:
        setattr(record, field, eval(newtext))
    db[key] = record
db.close()
