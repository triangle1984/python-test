#!/usr/bin/env python
from threading import Thread
import time, os, sys, shelve
x = 0
def timer():
    global x
    while 1:
        time.sleep(1)
        x += 1
def load():
    global x
    check = os.path.isfile("test")
    if check:
        with shelve.open("test") as stat:
            x = stat["maintime"]
            print(x)
def main():
    load()
    test = Thread(target=timer, daemon=True)
    test.start()
    run = " ".join(sys.argv[1:])
    os.system(run)
    print(x)
    with shelve.open("test") as stat:
        stat["maintime"] = x
main()
