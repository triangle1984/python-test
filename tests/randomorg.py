#!/usr/bin/env python 
from pprint import pprint
import requests
import json
url = "https://api.random.org/json-rpc/2/invoke"
mykey = "cb5861a7-60c0-4513-af5b-f8df81aa8e7e"
headers = {'content-type': 'application/json'}
data = {'jsonrpc':'2.0',
        'method':'generateIntegers','params':
        {'apiKey':mykey,
         'n':1
         ,'min':0
         ,'max':100}
        ,'id':24565}
params = json.dumps(data)
r = requests.post(url, params, headers=headers)
encode = r.json()
random = encode["result"]["random"]["data"]
print(random[0])
