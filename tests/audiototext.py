import speech_recognition as sr
from pydub import AudioSegment
from os import path

src = "test.mp3"
sound = AudioSegment.from_mp3(src)
sound.export("test.wav", format="wav")

audio_file = "test.wav" # путь до файла
r  = sr.Recognizer() # Использование файла как источник
with sr.AudioFile(audio_file) as source:
    audio = r.record(source) # Считывает весь файл
try:
    print("Сказал: " + r.recognize_google(audio, language = "ru_RU")) # в user_id вставиить переменную пользователся
except sr.UnknownValueError:
    print(f"Блять, говори четьче, не хуя ")
except sr.RequestError:
    print(f"Ошибка при отправки запроса")
