#!/usr/bin/env python 
from class2 import AttDisplay


class Person(AttDisplay):
    def __init__(self, name, job=None, pay=0):
        self.name = name
        self.job = job
        self.pay = pay

    def lastName(self):
        return self.name.split()[-1]

    def giveRaise(self, percent):
        self.pay = int(self.pay * (1 + percent))


class Manager(Person):
    def __init__(self, name, pay):
        Person.__init__(self, name, "Ньюбилов", pay)

    def giveRaise(self, percent, bonus=.10):
        Person.giveRaise(self, percent + bonus)


if __name__ == "__main__":
    print("Начало тестиирования..")
    artyr = Person("Артур Фантаз", job="Пограничник", pay=666)
    tom = Manager("Виктория Антонец", 5000)
    artyr.giveRaise(.10)
    tom.giveRaise(.10)
    print("--- Тута абсолютно все с прибавкой ---")
    for object in (artyr, tom):
        object.giveRaise(.10)
        print(object)
    print("Конец тестирования")
    print(Person.lastName(artyr))
