#!/usr/bin/env python 
import requests
from datetime import datetime
import os
def pogoda():
    apiurl = "http://api.openweathermap.org/data/2.5/find"
    appid = "22c7bf8e593c47b0cf88f390e8e5376a"
    params = {
            'q': "samara",
            'appid': appid,
            'units': 'metric',
            'lang': 'ru'
    }
    r = requests.get(apiurl, params=params, timeout=5)
    encode = r.json()
    return encode["list"][0]["main"]["temp"]
def time():
    now = datetime.now()
    return datetime.strftime(datetime.now(), "%H:%M:%S")
def cowsay():
    argv = None
    pogoda2 = pogoda()
    if int(pogoda2) > 25:
        argv = "-d"
    time2 = time()
    os.system("cowsay {} погода: {}C, время: {}".format(argv, pogoda2, time2))
cowsay()
