#!/usr/bin/env python 
class MainError(Exception): pass


class NomainError(MainError): pass


class AirError(NomainError): pass


def func():
    raise AirError()

func()
