import importlib
import importlib.util

def dynamic_import(module):
    return importlib.import_module(module)
def check_module(module_name):
    module_spec = importlib.util.find_spec(module_name)
    if module_spec is None:
        print('Module: {} not found'.format(module_name))
        return None
    else:
        print('Module: {} can be imported!'.format(module_name))
        return module_spec
if __name__ == '__main__':
    module = dynamic_import('module1')
    module.main()
    t = check_module("os")
    print(t)
