import pymysql
from pymysql.cursors import DictCursor
from contextlib import closing
# авторизация
with closing(pymysql.connect(host="10.59.111.63",
                             user="root",
                             password="123",
                             db="mydb",
                             cursorclass=DictCursor)) as conn:
    # потенциальные данные юзера с вк
    uid = 44810
    uname = "чорный властелинище"
    # получаем запись из бд по айдишнику
    with conn.cursor() as cursor:
        query = f"SELECT * FROM prefix WHERE id = '{uid}'"
        cursor.execute(query)
        # если нет, записать
        if cursor.fetchone() == None:
            print("запись")
            with conn.cursor() as cursor:
                query = f"INSERT INTO prefix (id, name) VALUES ({uid}, '{uname}')"
                cursor.execute(query)
                conn.commit()
        with conn.cursor() as cursor:
            query = f"UPDATE prefix SET name = 'не аирец' WHERE id = '{uid}'"
            cursor.execute(query)
            conn.commit()
        # в любом случае получить запись из бд, даже ежели ее не было и мы
        # только записали
        with conn.cursor() as cursor:
            query = f"SELECT * FROM prefix WHERE id = '{uid}'"
            cursor.execute(query)
            print(cursor.fetchone())
